import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int? count = 1;
  int? qnum = 0;
  int? opt1 = 0;
  int? opt2 = 0;
  int? opt3 = 0;
  int? opt4 = 0;

  void questionNum() {
    if (count! <= 4 && qnum! <= 4) {
      setState(() {
        qnum = qnum! + 1;
        count = count! + 1;
        opt1 = opt1! + 1;
        opt2 = opt2! + 1;
        opt3 = opt3! + 1;
        opt4 = opt4! + 1;
      });
    }
  }

  final List<String>? question = [
    "Q.1) Which option is correct : ",
    "Q.2) Flutter is a : ?",
    "Q.3) Value of 8 | 4 : ",
    "Q.4) 2 x 2 is ",
    "Q.5) 1 & 2 is "
  ];

  final List<String> option1 = ["Option A", "Framework", "1", "0", "0"];
  final List<String> option2 = ["Option B", "tool", "12", "2", "2"];
  final List<String> option3 = ["Option C", "language", "4", "4", "1"];
  final List<String> option4 = [
    "Don't know",
    "all of these",
    "None of these",
    "don't know",
    "3"
  ];

  int? flag1 = 1;
  int? flag2 = 2;
  int? flag3 = 3;
  int? flag4 = 4;
  int? flag5 = 5;

  void optionFun() {

    if(qnum! == 1 ) {

      flag
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "TECH QUIZ",
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
        centerTitle: true,
      ),
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Text("Question No. $count/10"),
                const SizedBox(
                  height: 30,
                ),
                Text(question![qnum!]),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 25,
                  width: 200,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {},
                      child: Text(option1[opt1!])),
                ),
                const SizedBox(
                  height: 15,
                ),
                SizedBox(
                  height: 25,
                  width: 200,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {},
                      child: Text(option2[opt2!])),
                ),
                const SizedBox(
                  height: 15,
                ),
                SizedBox(
                  width: 200,
                  height: 25,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {},
                      child: Text(option3[opt3!])),
                ),
                const SizedBox(
                  height: 15,
                ),
                SizedBox(
                  width: 200,
                  height: 25,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {},
                      child: Text(option4[opt4!])),
                ),
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: questionNum,
        child: const Text("NEXT"),
      ),
    );
  }
}

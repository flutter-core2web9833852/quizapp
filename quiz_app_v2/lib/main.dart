import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State {
  int? score = 0;

  List<Map> questionList = [
    {
      "question": "Who is the founder of Microsoft ?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "answerIndex": [2]
    },
    {
      "question": "Who is the founder of SpaceX ?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "answerIndex": [3]
    },
    {
      "question": "Who is the founder of Apple ?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "answerIndex": [0]
    },
    {
      "question": "Who is the founder of Amazon ?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "answerIndex": [1]
    },
    {
      "question": "Who is the founder of Tesla ?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "answerIndex": [3]
    }
  ];

  int opt = -1;
  int opt1 = -1;
  int opt2 = -1;
  int opt3 = -1;
  int opt4 = -1;

  bool questionScreen = true;
  int questionIndex = 0;

  void nextQuestion() {
    setState(() {
      questionIndex = questionIndex + 1;
      if (questionIndex <= questionList.length - 1) {
        opt = -1;
        opt1 = -1;
        opt2 = -1;
        opt3 = -1;
        opt4 = -1;
      }
    });

    if (questionIndex == questionList.length) {
      questionScreen = false;
    }
  }

  correctAns(int i) {
    opt1 = -1;
    opt2 = -1;
    opt3 = -1;
    opt4 = -1;

    if (i == questionList[questionIndex]["answerIndex"][0]) {
      opt = 1;
      score = score! + 1;
    } else if (i != questionList[questionIndex]["answerIndex"][0]) {
      opt = 0;
      if (questionIndex == 0) {
        opt3 = 1;
      } else if (questionIndex == 1 || questionIndex == 4) {
        opt4 = 1;
      } else if (questionIndex == 2) {
        opt1 = 1;
      } else if (questionIndex == 3) {
        opt2 = 1;
      }
    }

    if (i == 0)
      opt1 = opt;
    else if (i == 1)
      opt2 = opt;
    else if (i == 2)
      opt3 = opt;
    else if (i == 3) opt4 = opt;
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            "QUIZ APP",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
                color: Colors.orange),
          ),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Question : ",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                ),
                Text(
                  "${questionIndex + 1}/${questionList.length} ",
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.w500),
                )
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 380,
              height: 50,
              child: Text(
                "${questionList[questionIndex]["question"]}",
                style:
                    const TextStyle(fontSize: 23, fontWeight: FontWeight.w400),
              ),
            ),
            const SizedBox(
              height: 25,
            ),
            Container(
              width: 250,
              height: 40,
              child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      correctAns(0);
                    });
                  },
                  style: (opt1 == 1)
                      ? ButtonStyle(
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green))
                      : (opt1 == 0)
                          ? ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.red))
                          : ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.white)),
                  child: Text("A.${questionList[questionIndex]["options"][0]}",
                      style: const TextStyle(fontSize: 20))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 250,
              height: 40,
              child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      correctAns(1);
                    });
                  },
                  style: (opt2 == 1)
                      ? ButtonStyle(
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green))
                      : (opt2 == 0)
                          ? ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.red))
                          : ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.white)),
                  child: Text("B.${questionList[questionIndex]["options"][1]}",
                      style: const TextStyle(fontSize: 20))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 250,
              height: 40,
              child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      correctAns(2);
                    });
                  },
                  style: (opt3 == 1)
                      ? ButtonStyle(
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green))
                      : (opt3 == 0)
                          ? ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.red))
                          : ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.white)),
                  child: Text("C.${questionList[questionIndex]["options"][2]}",
                      style: const TextStyle(fontSize: 20))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 250,
              height: 40,
              child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      correctAns(3);
                    });
                  },
                  style: (opt4 == 1)
                      ? ButtonStyle(
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green))
                      : (opt4 == 0)
                          ? ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.red))
                          : ButtonStyle(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) => Colors.white)),
                  child: Text("D.${questionList[questionIndex]["options"][3]}",
                      style: const TextStyle(fontSize: 20))),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: nextQuestion,
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.orange,
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            "QUIZ APP",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
                color: Colors.orange),
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Congratulations",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have failed in the quiz test",
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Your Score is $score/5",
                style: const TextStyle(fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ),  
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}

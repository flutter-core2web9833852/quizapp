import 'package:flutter/material.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class SingleQuestions {
  final String? question;
  final List<String>? options;
  final int? answerIndex;

  const SingleQuestions({this.question, this.options, this.answerIndex});
}

class _QuizAppState extends State {
  List questionList = [
    const SingleQuestions(
        question: "Linux is developed by : ",
        options: [
          "Steve Jobs",
          "Dennis Ritchie",
          "Linus Torvalds",
          "Bill Gates"
        ],
        answerIndex: 2),
    const SingleQuestions(
        question: "C Language is created by :",
        options: [
          "Guido van Rossum",
          "James Gosling",
          "Dennis Ritchie",
          "none of these"
        ],
        answerIndex: 2),
    const SingleQuestions(
        question: "Which is not an OOPs language ?",
        options: ["C", "C++", "Java", "Python"],
        answerIndex: 0),
    const SingleQuestions(
        question: "Unix OS is developed by ?",
        options: [
          "Dennis Ritchie",
          "Ken Thompson",
          "Both A and B",
          "none of these"
        ],
        answerIndex: 2),
    const SingleQuestions(
        question: "Unix OS is written in which language ?",
        options: ["Assembly Lang.", "C Language", "C++", "All of these"],
        answerIndex: 1)
  ];

  int questionScreen = 0;
  int questionNum = 1;
  int questionIndex = 0;
  int selectedAnsIndex = -1;
  int score = 0;
  int alert = 0;

  void nextQuestion() {
    if (selectedAnsIndex == -1) {
      alert = 1;
      return;
    } else if (questionNum <= questionList.length - 1) {
      questionIndex = questionIndex + 1;
      questionNum = questionNum + 1;
      selectedAnsIndex = -1;
    } else {
      questionScreen = 2;
    }
  }

  MaterialStateProperty<Color?> checkAns(int buttonIndex) {
    if (selectedAnsIndex != -1) {
      alert = 0;
      if (buttonIndex == questionList[questionIndex].answerIndex) {
        score = score + 2;
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedAnsIndex) {
        score = score - 2;
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == 1) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            "QUIZ APP",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
                color: Colors.orange),
          ),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Questions ",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
                ),
                Text(
                  "$questionNum/${questionList.length}",
                  style: const TextStyle(
                      fontSize: 22, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              "${questionList[questionIndex].question}",
              style: const TextStyle(fontSize: 23, fontWeight: FontWeight.w400),
            ),
            const SizedBox(
              height: 55,
            ),
            SizedBox(
              height: 50,
              width: 250,
              child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: checkAns(0)),
                  onPressed: () {
                    if (selectedAnsIndex == -1) {
                      setState(() {
                        selectedAnsIndex = 0;
                      });
                    }
                  },
                  child: Text("${questionList[questionIndex].options[0]}",
                      style: const TextStyle(
                          fontSize: 23, fontWeight: FontWeight.w400))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: 250,
              child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: checkAns(1)),
                  onPressed: () {
                    if (selectedAnsIndex == -1) {
                      setState(() {
                        selectedAnsIndex = 1;
                      });
                    }
                  },
                  child: Text("${questionList[questionIndex].options[1]}",
                      style: const TextStyle(
                          fontSize: 23, fontWeight: FontWeight.w400))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: 250,
              child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: checkAns(2)),
                  onPressed: () {
                    if (selectedAnsIndex == -1) {
                      setState(() {
                        selectedAnsIndex = 2;
                      });
                    }
                  },
                  child: Text("${questionList[questionIndex].options[2]}",
                      style: const TextStyle(
                          fontSize: 23, fontWeight: FontWeight.w400))),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: 250,
              child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: checkAns(3)),
                  onPressed: () {
                    if (selectedAnsIndex == -1) {
                      setState(() {
                        selectedAnsIndex = 3;
                      });
                    }
                  },
                  child: Text("${questionList[questionIndex].options[3]}",
                      style: const TextStyle(
                          fontSize: 23, fontWeight: FontWeight.w400))),
            ),
            (alert == 1)
                ? Container(
                    child: const Column(
                      children: [
                        SizedBox(
                          height: 75,
                        ),
                        Text(
                          "Please select an option !",
                          style: TextStyle(
                              fontSize: 23,
                              fontWeight: FontWeight.w500,
                              color: Colors.red),
                        ),
                      ],
                    ),
                  )
                : Container(),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              nextQuestion();
            });
          },
          child: const Icon(
            Icons.forward_rounded,
            color: Colors.orange,
          ),
        ),
      );
    } else if (questionScreen == 2) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            "QUIZ APP",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
                color: Colors.orange),
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 30,
              ),
              Image.network(
                "https://img.freepik.com/premium-vector/winner-trophy-cup-with-ribbon-confetti_51486-122.jpg",
                height: 300,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have completed the quiz test",
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Your Score is $score/10",
                style:
                    const TextStyle(fontWeight: FontWeight.w500, fontSize: 23),
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    questionNum = 1;
                    questionIndex = 0;
                    selectedAnsIndex = -1;
                    score = 0;
                    alert = 0;
                    questionScreen = 0;
                  });
                },
                child: const Text("Restart",
                    style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.w400)),
              )
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            "QUIZ APP",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
                color: Colors.orange),
          ),
        ),
        body: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              Image.network(
                "https://t4.ftcdn.net/jpg/06/35/40/61/360_F_635406128_DWuMlkWllkPyn3x49lIWgcOhLXJOb79J.jpg",
                height: 300,
              ),
              const SizedBox(
                height: 30,
              ),
              const Text("Click the button to Start the Quiz",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w400)),
              const SizedBox(
                height: 75,
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        questionScreen = 1;
                      });
                    },
                    child: const Text("Start",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w400))),
              )
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
